CREATE DATABASE IF NOT EXISTS tournaments DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE tournaments;

SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS teams_users_ref;
DROP TABLE IF EXISTS teams;
DROP TABLE IF EXISTS tournaments;
DROP TABLE IF EXISTS stages;
DROP TABLE IF EXISTS battles;
DROP TABLE IF EXISTS t_stage;
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE users (
	id CHAR(36) PRIMARY KEY NOT NULL,
	name VARCHAR(200)
) ENGINE=InnoDB;

CREATE TABLE teams_users_ref (
	user_id CHAR(36),
	team_id CHAR(36)
) ENGINE=InnoDB;

CREATE TABLE teams (
	id CHAR(36) PRIMARY KEY NOT NULL,
	tournament_id CHAR(36) NOT NULL,
	name VARCHAR(200),
	state CHAR(10)
) ENGINE=InnoDB;

CREATE TABLE tournaments (
	id CHAR(36) PRIMARY KEY NOT NULL,
	name VARCHAR(200),
	description VARCHAR(2000),
	begin_date DATETIME,
	deadline DATETIME,
	state CHAR(10),
	organizer CHAR(36) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE t_stage (
	id CHAR(10)
) ENGINE=InnoDB;

CREATE TABLE stages (
	id CHAR(36) PRIMARY KEY NOT NULL,
	tournament_id CHAR(36),
	next_stage_id CHAR(36)
) ENGINE=InnoDB;

CREATE TABLE battles (
	id CHAR(36) PRIMARY KEY NOT NULL,
	stage_id CHAR(36),
	left_team_id CHAR(36),
	right_team_id CHAR(36),
	left_proof BLOB,
	right_proof BLOB,
	next_battle_id CHAR(36),
	deadline DATETIME
) ENGINE=InnoDB;

COMMIT;

ALTER TABLE teams_users_ref
	ADD FOREIGN KEY (user_id)
	REFERENCES users(id)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION;

ALTER TABLE teams_users_ref
	ADD FOREIGN KEY (team_id)
	REFERENCES teams(id)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION;
	
ALTER TABLE teams
	ADD FOREIGN KEY (tournament_id)
	REFERENCES tournaments(id)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION;
	
ALTER TABLE tournaments
	ADD FOREIGN KEY (organizer)
	REFERENCES users(id)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION;
	
ALTER TABLE tournaments
	ADD FOREIGN KEY (stage)
	REFERENCES t_stage(id)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION;
	
ALTER TABLE stages
	ADD FOREIGN KEY (tournament_id)
	REFERENCES tournaments(id)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION;

ALTER TABLE stages
	ADD FOREIGN KEY (next_stage_id)
	REFERENCES stages(id)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION;
	
ALTER TABLE battles
	ADD FOREIGN KEY (stage_id)
	REFERENCES stages(id)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION;
	
ALTER TABLE battles
	ADD FOREIGN KEY (left_team_id)
	REFERENCES teams(id)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION;
	
ALTER TABLE battles
	ADD FOREIGN KEY (right_team_id)
	REFERENCES teams(id)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION;

ALTER TABLE battles
	ADD FOREIGN KEY (next_battle_id)
	REFERENCES battles(id)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION;
	
INSERT INTO t_stage (id) VALUES ('CREATED');
INSERT INTO t_stage (id) VALUES ('PUBLISHED');
INSERT INTO t_stage (id) VALUES ('STARTED');
INSERT INTO t_stage (id) VALUES ('FINISHED');
INSERT INTO t_stage (id) VALUES ('ARCHIVE');